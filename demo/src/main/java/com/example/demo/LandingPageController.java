package com.example.demo;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LandingPageController {

	@GetMapping("/")
	public String index() {
		return "Current time: " + new SimpleDateFormat("yyy-MM-dd 'at' HH:mm:ss z").format(new Date(System.currentTimeMillis()));
	}

}